package ass.vehicle.rented;

public class FuelVehicle extends RentedVehicle{
	//private  double MileageFees;
	private double nbKms;//total number of kilometers traveled

	
	public FuelVehicle(double baseFees, double nbKms) {
		super(baseFees);
		this.nbKms = nbKms;
	}

	public double getMileageFees() {
		if(nbKms<100) {
		   return 0.2*nbKms;
		
		}
		
		else if(nbKms<=400 || nbKms>=100) {
			return 0.3*nbKms;
		}
		
			return 0.3*400+0.5*(nbKms-400);
		
	}

	    
		
	

}
