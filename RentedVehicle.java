package ass.vehicle.rented;

public class RentedVehicle {
	private double baseFees;

	public RentedVehicle(double baseFees) {
		super();
		this.baseFees = baseFees;
	}

	public double getBaseFees() {
		return baseFees;
	}

	public void setBaseFees(double baseFees) {
		this.baseFees = baseFees;
	}

}
