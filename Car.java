package ass.vehicle.rented;

public class Car extends FuelVehicle{
	private int nbSeats;
	
	public Car(double baseFees, double nbKms) {
		super(baseFees, nbKms);
		
	}
	@Override
	public double getMileageFees() {
				return super.getMileageFees();
	}
	public int getNbSeats() {
		return nbSeats;
	}
	

	public void setNbSeats(int nbSeats) {
		this.nbSeats = nbSeats;
	}

	public Car(double baseFees, int nbSeats) {
		super(baseFees, nbSeats);
		this.nbSeats = nbSeats;
	}
	

	
	public double getCost() {
		return (nbSeats*getBaseFees())+getMileageFees();
		
	}
	
	
	

}
